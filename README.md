Compile MYSBootloader - MySensors bootloader supporting over-the-air (OTA) firmware updates


#####################################################
# Connect the board to USBasp
#####################################################
# MOSI -> D11
# MISO -> D12
# SCK  -> D13

# VCC -> VCC
# GND -> GND
# RST -> RST


#####################################################
# Grab source code from GitHub
#####################################################
git clone https://github.com/mysensors/MySensorsBootloaderRF24.git src


#####################################################
# Update MYS Bootloader channel
#####################################################
check 'RF24 communication settings' in MYSBootloader.c
#define RF24_CHANNEL 


#####################################################
# MYS Bootloader config for RFNano
#####################################################
Arduino Nano is configured with SPI having CE9 CSN10 

RF Nano requires 
#define MY_RF24_CE_PIN 10 
#define MY_RF24_CS_PIN 9

In order to make MYS Bootloader to work with RF Nano, 
open HW.h file, search for SPI_PINS_CE9_CSN10
then change CSN_PIN and CE_PIN


#####################################################
# Burn bootloader
#####################################################
Use 'Burn Bootloader' option from PlatformIO section